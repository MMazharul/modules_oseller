<?php $this->load->view('e_commerce/ecom_header_link'); ?>

<body class="cms-index-index cms-home-page">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Mobile Menu Start-->

    <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>

    <!-- Mobile Menu End -->


    <div id="page">

      <!-- Header Start -->

      <?php $this->load->view('e_commerce/ecom_header'); ?>
      <!-- Header End -->

      <?php $this->load->view('e_commerce/ecom_navbar'); ?>


      <!-- Breadcrumbs -->

 <div class="breadcrumbs">
   <div class="container">
     <div class="row">
       <div class="col-xs-12">
         <ul>
           <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>

           <li><strong>Checkout</strong></li>
         </ul>
       </div>
     </div>
   </div>
 </div>
 <!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col2-right-layout">
 <div class="main container">
   <div class="row">
     <div class="col-main col-sm-8 col-xs-12">

<form action="<?=base_url('/ecom_home/checkOutStore')?>" method="post">
       <div class="page-content checkout-page"><div class="page-title">
         <h2>Checkout</h2>
       </div>
           <div class="box-border">
               <div class="row">
                 <div class="col-sm-6 ">
                     <h5>Shipping Address</h5>
                     <div class="checkbox">
                       <label>

                       </label>
                      </div>

                     <label>Name</label>
                     <input type="text" id="name" class="form-control input" name="shipping_name">
                     <label>Phone</label>
                     <input type="text" id="phone" class="form-control input" name="shipping_phone">

                     <label>Alternative Phone</label>
                     <input type="text" id="alternate_phone" class="form-control input" name="shipping_alternate_phone">

                     <label>Division</label>
                     <select class="form-control"  id="shipping_division" name="shipping_division">
                       <option disabled selected>Select Division</option>
                       <?php
                       foreach($divisions as $division)
                       {
                       ?>
                       <option value="<?=$division['id']?>"><?=$division['name']?></option>
                       <?php
                         }
                       ?>
                     </select>

                     <label>District</label>
                     <select class="form-control" id="district1" name="shipping_district">
                         <option disabled selected>Select District</option>
                         <option>Dhaka</option>
                     </select>

                     <label>Area</label>
                     <select class="form-control" id="area1" name="shipping_area">
                       <option disabled selected>Select Area</option>
                         <option>Dhaka</option>
                     </select>

                     <label>Email</label>
                     <input type="text" id="email" class="form-control input" name="shipping_email">

                     <label>Contact Address</label>
                     <textarea cols="5" rows="5" id="address" name="shipping_address" class="form-control input"></textarea>

                 </div>
                  <div class="col-sm-6">
                      <h5>Billing Address</h5>
                     <div class="checkbox">
                       <label>
                         <input id="check" onclick="sameShippingAdrees()" type="checkbox">same as shipping address
                       </label>
                      </div>

                       <label>Name</label>
                       <input type="text" id="name1" class="billing form-control input" name="billing_name">
                       <label>Phone</label>
                       <input type="text" id="phone1" name="billing_phone"  class="billing form-control input" name="billing_phone">

                       <label>Alternative Phone</label>
                       <input type="text" id="alternate_phone1" class="billing form-control input" name="billing_alternate_phone">

                       <label>Division</label>
                       <select class="billing form-control" id="billing_division" name="billing_division">
                         <option disabled selected>Select Division</option>
                          <?php
                          foreach($divisions as $division)
                          {
                          ?>
                          <option value="<?=$division['id']?>"><?=$division['name']?></option>
                          <?php
                            }
                          ?>

                       </select>

                       <label>District</label>
                       <select class="billing form-control" id="district2" name="billing_district">
                           <option disabled selected>Select District</option>
                           <option>Dhaka</option>
                       </select>

                       <label>Area</label>
                       <select class="form-control billing" id="area2" name="billing_area">
                         <option disabled selected>Select Area</option>
                           <option>Dhaka</option>
                       </select>

                       <label>Email</label>
                       <input type="text" id="email1" class="billing form-control input" name="billing_email">

                       <label>Contact Address</label>
                       <textarea cols="5" rows="5" id="address1" name="billing_address" class="billing form-control input"></textarea>
                   </div>
               </div>
           </div>

           <button type="submit" class="button"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Confirm</span></button>

       </div>
  </form>
     </div>

     <aside class="right sidebar col-sm-4 col-xs-12">
       <div class="card sidebar-checkout block">
        <div class="sidebar-bar-title">
             <h4 class="text-center">Checkout Summary</h3>
           </div>

           <div class=" block-content">

             <h4>Payment Details</h4>
             <table class="table table-condensed">
               <tbody>
                 <tr><td>Subtotal</td><td>330</td></tr>
                 <tr><td>Shipping</td><td>50</td></tr>
                 <tr><td>Total</td><td>380</td></tr>
                 <tr><td>Payable Total</td><td>380</td></tr>
               </tbody>
             </table>


           </div>
       </div>
       </aside>
   </div>
 </div>
 </section>
 <!-- Footer Starts -->
 <?php $this->load->view('e_commerce/ecom_footer'); ?>
 <!-- Footer Ends -->

 <!-- Footer Link Starts -->
 <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
 <!-- Footer Link Ends -->

<script>
    $('#shipping_division').on('change', function(e)
    {
    var division_id = e.target.value;
     getDistrict(division_id, 1);
    });

      $('#billing_division').on('change', function(e)
      {
        var division_id = e.target.value;
        getDistrict(division_id, 2)
      });

      $('#district1').on('change', function(e)
      {

      var district_id = e.target.value;

       getArea(district_id, 1);
      });

        $('#district2').on('change', function(e)
        {
          var district_id = e.target.value;
          getArea(district_id, 2)
        });

    function getDistrict(divisionId, step)
    {

        $.get('<?=base_url()?>home/findDistrict?division_id=' + divisionId,function(data)
        {
             let district = $('#district'+step);
             let districts = JSON.parse(data);
            district.empty();
            district.append('<option value="0" disabled="true" selected="true">select district</option>');

           for(let districtData of districts)
           {
             district.append('<option value="'+ districtData.id +'">'+districtData.name+'</option>');
           }
        });
    }

    function getArea(districtId, step)
    {
        $.get('<?=base_url()?>home/findArea?district_id=' + districtId,function(data)
        {
             let area = $('#area'+step);
             let allData = JSON.parse(data);
            area.empty();
            area.append('<option value="0" disabled="true" selected="true">select Area</option>');

           for(let singleData of allData)
           {
            area.append('<option value="'+ singleData.area_title +'">'+singleData.area_title+'</option>');
           }
        });
    }

    function sameShippingAdrees() {
      let division2 =$('#shipping_division :selected').text();
      let district2 = $('#district1 :selected').text();
      let area = $('#area1 :selected').text();
      let email = $('#email').val();
      let name = $('#name').val();
      let phone = $('#phone').val();
      let alternate_phone = $('#alternate_phone').val();
      let shippingAddress=$("textarea#address").val();

            if ($("#check").is(':checked')) {
              if(division2!= '' &&  district2!='' && area!='' && email!='' && name!='' && phone!='' && alternate_phone!='' && shippingAddress!='')
              {
                let divisionValue =$('#shipping_division :selected').val();
                $('#billing_division').html("<option value="+ divisionValue +">" + division2 + "<option>");
                let districtValue = $('#district1 :selected').val();
                $('#district2').html("<option value="+districtValue+">" + district2 + "<option>");
                let areaValue = $('#area1 :selected').val();
                $('#area2').html("<option value="+areaValue+">" + areaValue + "<option>");
                $('#email1').val(email);
                $('#name1').val(name);
                $('#phone1').val(phone);
                $('#alternate_phone1').val(alternate_phone);
                $('#address1').html(shippingAddress);

                $(".billing").attr("readonly", true);
              }
              else {
                alert('fillup your shipping address');
                $("#check").prop("checked", false);
              }


            } else {
                $('#billing_division').html(" <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " <?php foreach($divisions as $division) {?>\n" + "  <option value=\"<?= $division['id']?>\"><?= $division['name'] ?></option>\n" + " <?php } ?>");
                $('#district2').html("<option><option>");
                $('#area2').html("<option><option>");
                $('#phone1').val('');
                $('#email1').val('');
                $('#name1').val('');
                $('#alternate_phone1').val('');
                // $('textarea#address1').val('');
                $('textarea#address1').text('');
                $(".billing").attr("readonly", false);
            }
        }
        $('#cart_content').load("<?php echo base_url(); ?>cart/view");

</script>
 <!-- Main Container End -->
