<?php $this->load->view('e_commerce/ecom_header_link'); ?>

<body class="cms-index-index cms-home-page">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Mobile Menu Start-->

    <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>

    <!-- Mobile Menu End -->


    <div id="page">

      <!-- Header Start -->

      <?php $this->load->view('e_commerce/ecom_header'); ?>
      <!-- Header End -->

      <!-- Navbar Start -->
      <section class="main-container col1-layout">
        <div class="main container">
          <div class="col-main">
            <div class="cart">

              <div class="page-content page-order"><div class="page-title">
                <h2>Shopping Cart</h2>
              </div>


                <div class="order-detail-content">
                  <div class="table-responsive" id="table">
                    <!-- <table class="table table-bordered cart_summary">
                      <thead>
                        <tr>
                          <th class="cart_product">Product</th>
                          <th>Description</th>
                          <th>Avail.</th>
                          <th>Unit price</th>
                          <th>Qty</th>
                          <th>Total</th>
                          <th  class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="cart_product"><a href="#"><img src="images/products/img01.jpg" alt="Product"></a></td>
                          <td class="cart_description"><p class="product-name"><a href="#">Ipsums Dolors Untra </a></p>
                            <small><a href="#">Color : Red</a></small><br>
                            <small><a href="#">Size : M</a></small></td>
                          <td class="availability in-stock"><span class="label">In stock</span></td>
                          <td class="price"><span>$49.88</span></td>
                          <td class="qty"><input class="form-control input-sm" type="text" value="1"></td>
                          <td class="price"><span>$49.88</span></td>
                          <td class="action"><a href="#"><i class="icon-close"></i></a></td>
                        </tr>
                        <tr>
                          <td class="cart_product"><a href="#"><img src="images/products/img02.jpg" alt="Product"></a></td>
                          <td class="cart_description"><p class="product-name"><a href="#">Ipsums Dolors Untra </a></p>
                            <small><a href="#">Color : Green</a></small><br>
                            <small><a href="#">Size : XL</a></small></td>
                          <td class="availability out-of-stock"><span class="label">No stock</span></td>
                          <td class="price"><span>$00.00</span></td>
                          <td class="qty"><input class="form-control input-sm" type="text" value="0"></td>
                          <td class="price"><span>00.00</span></td>
                          <td class="action"><a href="#"><i class="icon-close"></i></a></td>
                        </tr>
                        <tr>
                          <td class="cart_product"><a href="#"><img src="images/products/img03.jpg" alt="Product"></a></td>
                          <td class="cart_description"><p class="product-name"><a href="#">Ipsums Dolors Untra </a></p>
                            <small><a href="#">Color : Blue</a></small><br>
                            <small><a href="#">Size : S</a></small></td>
                          <td class="availability in-stock"><span class="label">In stock</span></td>
                          <td class="price"><span>$99.00</span></td>
                          <td class="qty"><input class="form-control input-sm" type="text" value="2"></td>
                          <td class="price"><span>$188.00</span></td>
                          <td class="action"><a href="#"><i class="icon-close"></i></a></td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="2" rowspan="2"></td>
                          <td colspan="3">Total products (tax incl.)</td>
                          <td colspan="2">$237.88 </td>
                        </tr>
                        <tr>
                          <td colspan="3"><strong>Total</strong></td>
                          <td colspan="2"><strong>$237.88 </strong></td>
                        </tr>
                      </tfoot>
                    </table> -->
                  </div>
                  <div class="cart_navigation"> <a class="continue-btn" href="<?=base_url('/ecom_home')?>"><i class="fa fa-arrow-left"> </i>&nbsp; Continue shopping</a> <a class="checkout-btn" href="<?=base_url('ecom_home/checkout')?>"><i class="fa fa-check"></i> Proceed to checkout</a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Footer Starts -->
      <?php $this->load->view('e_commerce/ecom_footer'); ?>
      <!-- Footer Ends -->

      <!-- Footer Link Starts -->
      <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
      <!-- Footer Link Ends -->
      <script>

      $(document).on('blur','#up',function(){

          var qtyValue = $(this).val();

          var rowid = $(this).attr("data-id");

          if(qtyValue)
          {
            $.ajax({
             url:"<?php echo base_url(); ?>ecom_home/update_quenty",
             method:"POST",
             data:{rowid:rowid,qty:qtyValue},
             success:function(data)
             {
              //alert("Quenty updated");
              $('#table').html(data);
             }
            });
          }
          else {
            $(this).val(1);

          }


      });

      $('#table').load("<?php echo base_url(); ?>ecom_home/cart_view");
      $('#cart_content').load("<?php echo base_url(); ?>cart/view");

      </script>
