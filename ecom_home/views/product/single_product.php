<?php $this->load->view('e_commerce/ecom_header_link'); ?>

<body class="cms-index-index cms-home-page">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Mobile Menu Start-->

    <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>

    <!-- Mobile Menu End -->


    <div id="page">

      <!-- Header Start -->

      <?php $this->load->view('e_commerce/ecom_header'); ?>
      <!-- Header End -->

      <?php $this->load->view('e_commerce/ecom_navbar'); ?>

      <div class="breadcrumbs">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul>
                <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
                <li class=""> <a title="Go to Home Page" href="shop_grid.html"><?=$singleProduct['category_name']?></a><span>&raquo;</span></li>
                <li><strong><?=$singleProduct['title']?></strong></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Breadcrumbs End -->
      <!-- Main Container -->
      <div class="main-container col1-layout">
      <div class="container">
        <div class="row">
          <div class="col-main">
            <div class="product-view-area">
              <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                <div class="icon-sale-label sale-left">Sale</div>
                <div class="large-image"> <a href="images/products/img03.jpg" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt="products"> </a> </div>
                <div class="flexslider flexslider-thumb">
                  <ul class="previews-list slides">
                    <li><a href='images/products/img01.jpg' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>' "><img src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt = "Thumbnail 2"/></a></li>
                    <li><a href='images/products/img07.jpg' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>' "><img src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt = "Thumbnail 1"/></a></li>
                    <li><a href='images/products/img02.jpg' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>' "><img src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt = "Thumbnail 1"/></a></li>
                    <li><a href='images/products/img03.jpg' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>' "><img src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt = "Thumbnail 2"/></a></li>
                    <li><a href='images/products/img04.jpg' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>' "><img src="<?=base_url('uploads/c2c/'.$singleProduct['feature_image'])?>" alt = "Thumbnail 2"/></a></li>
                  </ul>
                </div>

                <!-- end: more-images -->

              </div>
              <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">

                  <div class="product-name">
                    <h1><?=$singleProduct['title']?></h1>
                  </div>
                  <div class="price-box">
                    <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?=number_format($singleProduct['price'],2)?> &#x9f3; </span> </p>
                    <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $359.99 </span> </p>
                  </div>
                  <div class="ratings">
                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                    <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>
                    <p class="availability in-stock pull-right">Availability: <span>In Stock</span></p>
                  </div>
                  <div class="short-description">
                    <h2>Quick Overview</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum.
                    <p> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>

                  </div>
                  <!-- <div class="product-color-size-area">
                    <div class="color-area">
                      <h2 class="saider-bar-title">Color</h2>
                      <div class="color">
                        <ul>
                          <li><a href="#"></a></li>
                          <li><a href="#"></a></li>
                          <li><a href="#"></a></li>
                          <li><a href="#"></a></li>
                          <li><a href="#"></a></li>
                          <li><a href="#"></a></li>
                        </ul>
                      </div>
                    </div>

                  </div> -->
                  <div class="product-variation">
                    <form action="#" method="post">
                      <div class="cart-plus-minus">
                        <label for="qty">Quantity:</label>
                        <div class="numbers-row">
                          <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                          <input type="text" class="qty" title="Qty" value="1" maxlength="" id="qty" name="qty">
                          <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                        </div>
                      </div>
                      <button class="button pro-add-to-cart" onclick="adToCart(<?=$singleProduct['id']?>)" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                    </form>
                  </div>
                  <div class="product-cart-option">
                    <ul>
                      <li>
                       <?php if($exist_fav==0)
                       {?>
                       <a name="<?=$singleProduct['id']?>" id="wishlist" ><i class="fa fa-heart"></i><span>Favourite</span></a>
                     <?php }
                     else
                     {
                      ?>
                      <a name="<?=$singleProduct['id']?>" id="wishlist" ><i class="fa fa-heart"></i><span>Unfavourite</span></a>

                      <?php
                     }
                      ?>
                     </li>
                      <li><a href="#"><i class="fa fa-retweet"></i><span>Add to Compare</span></a></li>
                      <li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                    </ul>

                </div>
              </div>
            </div>
          </div>
          <div class="product-overview-tab">
            <div class="container">
              <div class="row">
                <div class="col-xs-12"><div class="product-tab-inner">
                  <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"> <a href="#description" data-toggle="tab"> Description </a> </li>                <li> <a href="#reviews" data-toggle="tab">Reviews</a> </li>
                     <li><a href="#product_tags" data-toggle="tab">Tags</a></li>
                    <li> <a href="#custom_tabs" data-toggle="tab">Custom Tab</a> </li>
                  </ul>
                  <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="description">
                      <div class="std">
                        <p><?=html_entity_decode(htmlspecialchars_decode($singleProduct['description']));?></p>
                      </div>
                    </div>


                      <div id="reviews" class="tab-pane fade">
                  <div class="col-sm-5 col-lg-5 col-md-5">
                    <div class="reviews-content-left">
                      <h2>Customer Reviews</h2>
                      <div class="review-ratting">
                      <table>
                        <tbody>
                          <?php
                          foreach ($productReviews as $key => $review) {
                           ?>
                          <tr>
                            <td>
                            <p class="author">
                              <?=$review['username']?>  <?=$review['review']?> <br><small><?=$review['created_at']?></small><br>
                              <div class="rating"><?=$review['rating']?><i class="fa fa-star"></i></div>
                            </p>
                          </td>
                        </tr>

                      <?php } ?>
                      </tbody>
                    </table>

                      </div>
                    </div>
                  </div>
                  <div class="col-sm-7 col-lg-7 col-md-7">
                    <div class="reviews-content-right">
                      <h2>Write Your Own Review</h2>
                      <form action="<?=base_url('/ecom_home/productReviewPost')?>" method="post">

                          <div class="table-responsive reviews-table">
                        <table>
                          <tbody><tr>
                            <th></th>
                            <th>1 star</th>
                            <th>2 stars</th>
                            <th>3 stars</th>
                            <th>4 stars</th>
                            <th>5 stars</th>
                          </tr>
                          <tr>
                            <td>Rating</td>
                            <td><input type="radio" name="rating" value="1"></td>
                            <td><input type="radio" name="rating" value="2"></td>
                            <td><input type="radio" name="rating" value="3"></td>
                            <td><input type="radio" name="rating" value="4"></td>
                            <td><input type="radio" name="rating" value="5"></td>
                          </tr>
                        </tbody></table></div>
                        <div class="form-area">
                          <div class="form-element">
                            <label>Review <em>*</em></label>
                            <textarea name="review"></textarea>
                          </div>
                          <input type="hidden" value="<?=$singleProduct['id']?>" name="ad_id">
                          <input type="hidden" value="<?=current_url();?>" name="url" >
                          <div class="buttons-set">
                            <button class="button submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;Review</span></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                    <div class="tab-pane fade" id="product_tags">
                      <div class="box-collateral box-tags">
                        <div class="tags">


                          <form id="addTagForm" action="#" method="get">
                            <div class="form-add-tags">


                              <div class="input-box"><label for="productTagName">Add Your Tags:</label>
                                <input class="input-text" name="productTagName" id="productTagName" type="text">
                                <button type="button" title="Add Tags" class="button add-tags"><i class="fa fa-plus"></i> &nbsp;<span>Add Tags</span> </button>
                              </div>
                              <!--input-box-->
                            </div>
                          </form>
                        </div>
                        <!--tags-->
                        <p class="note">Use spaces to separate tags. Use single quotes (') for phrases.</p>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="custom_tabs">
                      <div class="product-tabs-content-inner clearfix">
                        <p><strong>Lorem Ipsum</strong><span>&nbsp;is
                          simply dummy text of the printing and typesetting industry. Lorem Ipsum
                          has been the industry's standard dummy text ever since the 1500s, when
                          an unknown printer took a galley of type and scrambled it to make a type
                          specimen book. It has survived not only five centuries, but also the
                          leap into electronic typesetting, remaining essentially unchanged. It
                          was popularised in the 1960s with the release of Letraset sheets
                          containing Lorem Ipsum passages, and more recently with desktop
                          publishing software like Aldus PageMaker including versions of Lorem
                          Ipsum.</span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div></div>
            </div>
          </div>







        </div>
      </div>

      <!-- Related Product Slider -->

        <div class="container">
        <div class="row">
        <div class="col-xs-12">
         <div class="related-product-area">
       <div class="page-header">
              <h2>Related Products</h2>
            </div>
            <div class="related-products-pro">
                      <div class="slider-items-products">
                        <div id="related-product-slider" class="product-flexslider hidden-buttons">
                          <div class="slider-items slider-width-col4 fadeInUp">
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="icon-sale-label sale-left">Sale</div>
                                  <div class="icon-new-label new-right">New</div>
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img12.jpg" alt=""> <img class="hover-img" src="images/products/img12.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="icon-sale-label sale-left">Sale</div>
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img15.jpg" alt=""> <img class="hover-img" src="images/products/img15.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box">
                                          <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                          <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img03.jpg" alt=""> <img class="hover-img" src="images/products/img03.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box">
                                          <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                          <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img04.jpg" alt=""> <img class="hover-img" src="images/products/img04.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>

                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img05.jpg" alt=""> <img class="hover-img" src="images/products/img05.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box">
                                          <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                          <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-item">
                              <div class="item-inner fadeInUp">
                                <div class="product-thumbnail">
                                  <div class="pr-img-area"> <img class="first-img" src="images/products/img06.jpg" alt=""> <img class="hover-img" src="images/products/img06.jpg" alt="">
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                  </div>
                                  <div class="pr-info-area">
                                    <div class="pr-button">
                                      <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                      <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                      <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="item-info">
                                  <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                      <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                      <div class="item-price">
                                        <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div></div>

                      </div>
                      </div>
                    </div>
                    </div>
      <!-- Related Product Slider End -->


      <!-- Footer Starts -->
      <?php $this->load->view('e_commerce/ecom_footer'); ?>
      <!-- Footer Ends -->

      <!-- Footer Link Starts -->
      <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
      <!-- Footer Link Ends -->
      <script>


      function adToCart(id)
      {
        let qty = $('#qty').val();


        $.ajax({
         url:"<?php echo base_url(); ?>cart/add",
         method:"POST",
         data:{ad_id:id,qty:qty},
         success:function(data)
         {
           // $.notify("added into your cart",{autoHide:true,clickToHide:false,className:'success',style:'bootstrap',position:'bottom left'});
           $.notify("added into your cart.",'success');
           // $(".notifyjs-container").attr("style", "width:350px");
           $('#cart_content').html(data);

         }
        });
       }

       $(document).on('click','#wishlist',function(){

           let ad_id = $(this).attr('name');
           var login_id="<?=$this->session->userdata('login_id');?>";
           if(login_id=='')
           {
              $.notify("Please first login Now.",'error');
           }
           else {
             $.ajax({
              url:"<?php echo base_url(); ?>ecom_home/add_favourite",
              method:"POST",
              data:{ad_id:ad_id},
              success:function(data)
              {
                if(data==0)
                {
                    $("#wishlist").html('<i class="fa fa-heart"></i><span>Favourite</span>');
                     $.notify("Ad's removed from favourite list.",'warn');

                }
                else {
                  $("#wishlist").html('<i class="fa fa-heart"></i><span>Unfavourite</span>');
                  $.notify("Ad's added from favourite list.",'info');
                }


              }
             });
           }
       });

        $('#cart_content').load("<?php echo base_url(); ?>cart/view");

      </script>
