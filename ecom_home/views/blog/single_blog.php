<?php $this->load->view('e_commerce/ecom_header_link'); ?>

<body class="cms-index-index cms-home-page">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Mobile Menu Start-->

    <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>

    <!-- Mobile Menu End -->


    <div id="page">

      <!-- Header Start -->

      <?php $this->load->view('e_commerce/ecom_header'); ?>
      <!-- Header End -->

      <?php $this->load->view('e_commerce/ecom_navbar'); ?>

      <section class="blog_post">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-9">
              <div class="entry-detail">
                <div class="page-title">
                  <h1><?=$singleBlogInfo[0]['title']?></h1>
                </div>
                <div class="entry-photo">
                  <figure><img src="<?=base_url('uploads/blog/'.$singleBlogInfo[0]['image'])?>" alt="Blog"></figure>
                </div>
                <div class="entry-meta-data"> <span class="author"> <i class="fa fa-user"></i>&nbsp; by: <a href="#"><?=$singleBlogInfo[0]['username']?></a></span> <span class="cat"> <i class="fa fa-folder"></i>&nbsp; <a href="#"><?=$singleBlogInfo[0]['category_name']?></span> <span class="comment-count"> <i class="fa fa-comment"></i>&nbsp; 3 </span> <span class="date"><i class="fa fa-calendar">&nbsp;</i>&nbsp; <?=$singleBlogInfo[0]['created_at'];?></span>
                  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>&nbsp; <span>(5 votes)</span></div>
                </div>
                <div class="content-text clearfix">
                  <p><?=$singleBlogInfo[0]['description']?></p>
                </div>

              </div>
              <!-- Related Posts -->
              <div class="single-box">
                <h2>Related Posts</h2>
                <div class="slider-items-products">
                  <div id="related-posts" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4 fadeInUp">
                      <?php
                      foreach ($blogs as $blog) {

                      ?>
                      <div class="product-item">
                        <article class="entry">
                          <div class="entry-thumb image-hover2"> <a href="#"> <img src="<?=base_url('uploads/blog/'.$blog['image'])?>" width="251px" height="139px" alt="Blog"> </a> </div>
                          <div class="entry-info">
                            <h3 class="entry-title"><a href="#"><?=$blog['title']?></a></h3>
                            <div class="entry-meta-data"> <span class="comment-count"> <i class="fa fa-comment-o">&nbsp;</i> 1 </span> <span class="date"> <i class="fa fa-calendar">&nbsp;</i> 2015-12-05 </span> </div>
                            <div class="entry-more"> <a href="<?=base_url('ecom_home/singleblog/'.$blog['id'])?>">Read more</a> </div>
                          </div>
                        </article>
                      </div>
                      <?php
                      }
                      ?>

                    </div>
                  </div>
                </div>
              </div>
              <!-- ./Related Posts -->
              <!-- Comment -->


              <!-- ./Comment -->
            </div>
            <!-- right colunm -->

            <!-- ./right colunm -->
          </div>
        </div>
      </section>

      <!-- Navbar Start -->


      <!-- Footer Starts -->
      <?php $this->load->view('e_commerce/ecom_footer'); ?>
      <!-- Footer Ends -->

      <!-- Footer Link Starts -->
      <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
      <!-- Footer Link Ends -->
      <script>
              $('#cart_content').load("<?php echo base_url(); ?>cart/view");
      </script>
