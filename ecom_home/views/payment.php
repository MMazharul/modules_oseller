<?php $this->load->view('e_commerce/ecom_header_link'); ?>
<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 1px solid #ddd;
}
</style>
<body style="background:none">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Mobile Menu Start-->

    <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>

    <!-- Mobile Menu End -->


    <div id="">

      <!-- Header Start -->

      <?php $this->load->view('e_commerce/ecom_header'); ?>
      <!-- Header End -->

      <!-- Navbar Start -->
      <section class="main-container col1-layout">
        <div class="main container">
          <div class="row">
      <div class="col-xs-12">
      <div class="invoice-title">
        <h2>Invoice</h2><h3 class="pull-right">Order # <?=$invoiceId?></h3>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-6">
          <address>
          <strong>Billed To:</strong><br>
            <?=$userInfo['billing_name']?><br>
            Phone  : <?=$userInfo['billing_phone']?><br>
            Address: <?=$userInfo['billing_district']?>,<?=$userInfo['billing_area']?>.<br>

          </address>
        </div>
        <div class="col-xs-6 text-right">
          <address>
            <strong>Shipped To:</strong><br>
            <?=$userInfo['shipping_name']?><br>
            Phone  : <?=$userInfo['shipping_phone']?><br>
            Address: <?=$userInfo['shipping_district']?>,<?=$userInfo['shipping_area']?>.<br>

          </address>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6">

        </div>
        <div class="col-xs-6 text-right">
          <address>
            <strong>Order Date:</strong><br>
            <?=$userInfo['order_date']?><br><br>
          </address>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><strong>Order summary</strong></h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-condensed">
              <thead>
                    <tr>
                      <td><strong>Item</strong></td>
                      <td class="text-center"><strong>Price</strong></td>
                      <td class="text-center"><strong>Quantity</strong></td>
                      <td class="text-right"><strong>Totals</strong></td>
                    </tr>
              </thead>
              <tbody>
                <?php
                 foreach($orders as $order)
                 {
                 ?>
                <tr>
                    <td>BS-200</td>
                    <td class="text-center">&#x9f3 <?=number_format($order['price'],2)?></td>
                    <td class="text-center"><?=$order['qty']?></td>
                    <td class="text-right">&#x9f3 <?=number_format($order['subtotal'],2)?></td>
                </tr>
                <?php
                }
                ?>


                <tr>
                  <td class="thick-line"></td>
                  <td class="thick-line"></td>
                  <td class="thick-line text-center"><strong>Subtotal</strong></td>
                  <td class="thick-line text-right">&#x9f3 <?=number_format($userInfo['grand_total'],2)?></td>
                </tr>
                <tr>
                  <td class="no-line"></td>
                  <td class="no-line"></td>
                  <td class="no-line text-center"><strong>Shipping</strong></td>
                  <td class="no-line text-right">&#x9f3 15</td>
                </tr>
                <tr>
                  <td class="no-line"></td>
                  <td class="no-line"></td>
                  <td class="no-line text-center"><strong>Total</strong></td>
                  <td class="no-line text-right">&#x9f3 <?=number_format($userInfo['grand_total'],2)?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-md-11">
          <h5>Payment Status</h2>
        </div>
        <div class="col-md-1">
          <h5 >Unpaid</h2>
        </div>
        <div class="col-md-1 col-md-offset-11">
          <form onsubmit="return btn_disabled()" id="payment_gw" name="payment_gw" action="http://secure.aamarpay.com/index.php" method="post" >


                      <input type="hidden" name="cus_name" value="<?=$userInfo['shipping_name']?>">
                      <input type="hidden" name="cus_email" value="<?=$userInfo['shipping_email']?>">
                      <input type="hidden"  value="<?=$userInfo['shipping_phone']?>" name="cus_phone">


                      <input type="hidden" name ="opt_b" value=''>

                      <input type="hidden" name ="currency" value='BDT'>

                      <input type="hidden" id="store_id" name="store_id" value="osellers">
                      <input type="hidden" name="signature_key" value="707052fcbf66ed5431d626e9a38d2821">

                      <input type="hidden" name ="tran_id" value="<?=date('YmdHi').rand(1,9999999);?>">

                      <input type="hidden" name="amount" value ='1'>
                      <input type="hidden" name="desc" value="payment for ecommerse proudct">
                      <input type="hidden" name="success_url" value='https://osellers.com/payment/transaction_success'>
                      <input type="hidden" name="fail_url" value='https://osellers.com/payment/transaction_fail'>
                      <input type="hidden" name="cancel_url" value='https://osellers.com/payment/transaction_cancel'>

                        <input type="submit" class="btn btn-danger"  name="save" value="Payment">
              </form>

        </div>
      </div>
    </div>
  </div>
  </div>
        </div>
      </section>

      <!-- Footer Starts -->
      <?php $this->load->view('e_commerce/ecom_footer'); ?>
      <!-- Footer Ends -->

      <!-- Footer Link Starts -->
      <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
      <!-- Footer Link Ends -->
      <script>
        $('#cart_content').load("<?php echo base_url(); ?>cart/view");
      </script>
