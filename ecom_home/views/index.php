<?php $this->load->view('e_commerce/ecom_header_link'); ?>
<body class="cms-index-index cms-home-page">
   <!--[if lt IE 8]>
   <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
   <![endif]-->
   <!-- Mobile Menu Start-->
   <?php $this->load->view('e_commerce/ecom_mobile_menu'); ?>
   <!-- Mobile Menu End -->
   <div id="page">
   <!-- Header Start -->
   <?php $this->load->view('e_commerce/ecom_header'); ?>
   <!-- Header End -->
   <!-- Navbar Start -->
   <?php $this->load->view('e_commerce/ecom_navbar'); ?>
   <!-- Navbar End -->
   <!-- Slider Start -->
   <?php $this->load->view('e_commerce/ecom_slider'); ?>
   <!-- Slider End -->
   <!-- Banner Start-->
   <?php $this->load->view('e_commerce/ecom_banner'); ?>
   <!-- Banner End -->
   <!-- main container -->
   <div class="main-container col1-layout">
      <div class="container">
         <div class="row">
            <!-- Home Tabs  -->
            <div class="col-sm-8 col-md-9 col-xs-12">
               <div class="home-tab">
                  <ul class="nav home-nav-tabs home-product-tabs">
                     <li class="active"><a href="#featured" data-toggle="tab" aria-expanded="false">Flash Sale</a></li>
                     <li class="divider"></li>
                     <!--  <li> <a href="#top-sellers" data-toggle="tab" aria-expanded="false">Top Sellers</a> </li> -->
                  </ul>
                  <div id="productTabContent" class="tab-content">
                     <div class="tab-pane active in" id="featured">
                        <div class="featured-pro">
                           <div class="slider-items-products">
                              <div id="featured-slider" class="product-flexslider hidden-buttons">
                                 <div class="slider-items slider-width-col4">
                                    <div class="jtv-box-timer">
                                       <div class="countbox_1 jtv-timer-grid"></div>
                                    </div>
                                    <div class="product-item">
                                       <div class="item-inner">
                                          <div class="product-thumbnail">
                                             <div class="icon-sale-label sale-left">Sale</div>
                                             <div class="icon-new-label new-right">New</div>
                                             <div class="pr-img-area">
                                                <a title="Ipsums Dolors Untra" href="single_product.html">
                                                   <figure> <img class="first-img" src="<?=base_url()?>front_assets/e_commerce/images/products/img01.jpg" alt=""> <img class="hover-img" src="front_assets/e_commerce/images/products/img01.jpg" alt=""></figure>
                                                </a>
                                                <button type="button" class="add-to-cart-mt" onclick="adToCart(2)"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                             </div>
                                             <div class="pr-info-area">
                                                <div class="pr-button">
                                                   <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                                   <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                                   <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="item-info">
                                             <div class="info-inner">
                                                <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                                <div class="item-content">
                                                   <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                   <div class="item-price">
                                                      <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--Hot deal -->
            <div class="col-md-3 col-sm-4 col-xs-12 hot-products">
               <div class="hot-deal">
                  <span class="title-text">Hot deal</span>
                  <ul class="products-grid">
                     <li class="item">
                        <div class="product-item">
                           <div class="item-inner">
                              <div class="product-thumbnail">
                                 <div class="icon-hot-label hot-right">Hot</div>
                                 <div class="pr-img-area">
                                    <a title="Ipsums Dolors Untra" href="single_product.html">
                                       <figure> <img class="first-img" src="<?=base_url()?>front_assets/e_commerce/images/products/img15.jpg" alt=""> <img class="hover-img" src="front_assets/e_commerce/images/products/img15.jpg" alt=""></figure>
                                    </a>
                                    <button type="button" class="add-to-cart-mt" onclick="adToCart(3)"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                                 </div>
                                 <div class="jtv-box-timer">
                                    <div class="countbox_1 jtv-timer-grid"></div>
                                 </div>
                                 <div class="pr-info-area">
                                    <div class="pr-button">
                                       <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                       <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                       <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item-info">
                                 <div class="info-inner">
                                    <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html">Ipsums Dolors Untra </a> </div>
                                    <div class="item-content">
                                       <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                       <div class="item-price">
                                          <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end main container -->
   <!-- top banner -->
   <div class="banner-static">
      <div class="container">
         <div class="row">
            <div class="col-sm-4 col-sms-12">
               <div class="banner-box banner-box2">
                  <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/banner_staic2.png" alt=""></a>
                  <div class="box-hover">
                     <div class="banner-title">Fresh Strawberry</div>
                     <span>Save up to 55% off</span>
                  </div>
               </div>
            </div>
            <div class="col-sm-4 col-sms-12">
               <div class="banner-box banner-box1">
                  <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/banner_staic1.png" alt=""></a>
                  <div class="box-hover">
                     <div class="banner-title">Healthy Summer</div>
                     <span>Save up to 45% off</span>
                  </div>
               </div>
            </div>
            <div class="col-sm-4 col-sms-12">
               <div class="banner-box banner-box3">
                  <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/banner_staic3.png" alt=""></a>
                  <div class="box-hover">
                     <div class="banner-title">Fresh Salads</div>
                     <span>Welcome to Foodstore!</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--special-products-->
   <div class="container">
      <div class="special-products">
         <div class="page-header">
            <h2>special products</h2>
         </div>
         <div class="special-products-pro">
            <div class="slider-items-products">
               <div id="special-products-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4">
                    <?php
                    foreach ($products as $product) {

                    ?>
                     <div class="product-item">
                        <div class="item-inner">
                           <div class="product-thumbnail">
                              <div class="icon-sale-label sale-left">Sale</div>
                              <div class="icon-new-label new-right">New</div>
                              <div class="pr-img-area">
                                 <a title="Ipsums Dolors Untra" href="<?=base_url('ecom_home/singleproduct/'.$product['a_id'])?>">
                                    <figure> <img class="first-img" src="<?=base_url('uploads/c2c/'.$product['feature_image'])?>" alt=""> <img class="hover-img" src="<?=base_url('uploads/c2c/'.$product['feature_image'])?>"  alt=""></figure>
                                 </a>
                                 <input type="hidden" value="1" id="qty">
                                 <button type="button" onclick="adToCart(<?=$product['a_id']?>)" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                              </div>
                              <div class="pr-info-area">
                                 <div class="pr-button">
                                    <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                                    <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                                    <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                                 </div>
                              </div>
                           </div>
                           <div class="item-info">
                              <div class="info-inner">
                                 <div class="item-title"> <a title="Ipsums Dolors Untra" href="single_product.html"><?=$product['title']?></a> </div>
                                 <div class="item-content">
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                    <div class="item-price">
                                       <div class="price-box"> <span class="regular-price"> <span class="price"><?=number_format($product['price'],2)?> &#x9f3;</span> </span> </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php
                      }
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <!-- Testimonials Box -->
            <div class="testimonials">
               <div class="slider-items-products">
                  <div id="testimonials-slider" class="product-flexslider hidden-buttons home-testimonials">
                     <div class="slider-items slider-width-col4 ">
                        <div class="holder">
                           <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                              minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                              ex ea commodo consequat.
                           </p>
                           <div class="thumb"> <img src="<?=base_url()?>front_assets/e_commerce/images/testimonials-img3.jpg" alt="testimonials img"> </div>
                           <strong class="name">John Doe</strong> <strong class="designation">CEO, Company</strong>
                        </div>
                        <div class="holder">
                           <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                              ex ea commodo consequat. fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
                           </p>
                           <div class="thumb"> <img src="front_assets/e_commerce/images/testimonials-img1.jpg" alt="testimonials img"> </div>
                           <strong class="name">Vince Roy</strong> <strong class="designation">CEO, Newspaper</strong>
                        </div>
                        <div class="holder">
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                              minim veniam, quis nostrud.
                           </p>
                           <div class="thumb"> <img src="<?=base_url()?>front_assets/e_commerce/images/testimonials-img2.jpg" alt="testimonials img"> </div>
                           <strong class="name">John Doe</strong> <strong class="designation">CEO, ABC Softwear</strong>
                        </div>
                        <div class="holder">
                           <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                              minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                              ex ea commodo consequat.
                           </p>
                           <div class="thumb"> <img src="<?=base_url()?>front_assets/e_commerce/images/testimonials-img4.jpg" alt="testimonials img"> </div>
                           <strong class="name">Vince Roy</strong> <strong class="designation">CEO, XYZ Softwear</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End Testimonials Box -->
         <!-- our clients Slider -->
         <div class="col-md-6">
            <div class="our-clients">
               <div class="slider-items-products">
                  <div id="our-clients-slider" class="product-flexslider hidden-buttons">
                     <div class="slider-items slider-width-col6">
                        <!-- Item -->
                        <div class="item"> <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/brand1.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand2.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand11.png" alt="Image"></a></div>
                        <!-- End Item -->
                        <!-- Item -->
                        <div class="item"> <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/brand3.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand4.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand10.png" alt="Image"></a></div>
                        <!-- End Item -->
                        <!-- Item -->
                        <div class="item"> <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/brand5.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand6.png" alt="Image"></a><a href="#"><img src="front_assets/e_commerce/images/brand9.png" alt="Image"></a> </div>
                        <!-- End Item -->
                        <!-- Item -->
                        <div class="item"> <a href="#"><img src="<?=base_url()?>front_assets/e_commerce/images/brand7.png" alt="Image"></a> <a href="#"><img src="front_assets/e_commerce/images/brand3.png" alt="Image"></a><a href="#"><img src="front_assets/e_commerce/images/brand8.png" alt="Image"></a> </div>
                        <!-- End Item -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Latest news start -->
   <div class="container">
      <div id="latest-news" class="news">
         <div class="page-header">
            <h2>Latest news</h2>
         </div>
         <div class="slider-items-products">
            <div id="latest-news-slider" class="product-flexslider hidden-buttons">
               <div class="slider-items slider-width-col6">
                  <?php
                  foreach($blogs as $blog)
                  {
                  ?>
                  <div class="item">
                     <div class="jtv-blog">
                        <div class="blog-img"> <a href="single_post.html"> <img class="primary-img" src="<?=base_url('uploads/blog/'.$blog['image'])?>" width="348px" height="232px" alt=""></a> <span class="moretag"></span> </div>
                        <div class="blog-content-jtv">
                           <h2><a href="single_post.html"><?=$blog['title']?></a></h2>
                           <p><?=$blog['description']?></p>
                           <span class="blog-likes"><i class="fa fa-heart"></i> <?=$blog['total_like']?></span> <span class="blog-comments"><i class="fa fa-comment"></i> 80 comments</span>
                           <div class="blog-action"> <span>Jan, 20, 2017</span> <a class="read-more" href="<?=base_url('ecom_home/singleblog/'.$blog['id'])?>">read more</a> </div>
                        </div>
                     </div>
                  </div>
                  <?php
                  }
                  ?>

               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- service section -->
   <div class="jtv-service-area">
      <div class="container">
         <div class="row">
            <div class="col col-md-3 col-sm-6 col-xs-12">
               <div class="block-wrapper ship">
                  <div class="text-des">
                     <div class="icon-wrapper"><i class="fa fa-truck"></i></div>
                     <div class="service-wrapper">
                        <h3>World-Wide Shipping</h3>
                        <p>On order over $99</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col col-md-3 col-sm-6 col-xs-12 ">
               <div class="block-wrapper return">
                  <div class="text-des">
                     <div class="icon-wrapper"><i class="fa fa-refresh"></i></div>
                     <div class="service-wrapper">
                        <h3>30 Days Return</h3>
                        <p>Moneyback guarantee </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col col-md-3 col-sm-6 col-xs-12">
               <div class="block-wrapper support">
                  <div class="text-des">
                     <div class="icon-wrapper"><i class="fa fa-umbrella"></i></div>
                     <div class="service-wrapper">
                        <h3>SUPPORT 24/7</h3>
                        <p>Call us: ( +123 ) 456 789</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col col-md-3 col-sm-6 col-xs-12">
               <div class="block-wrapper user">
                  <div class="text-des">
                     <div class="icon-wrapper"><i class="fa fa-user"></i></div>
                     <div class="service-wrapper">
                        <h3>MEMBER DISCOUNT</h3>
                        <p>25% on order over $199</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- category area start -->
   <div class="jtv-category-area">
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-sm-6">
               <div class="jtv-single-cat">
                  <h2 class="cat-title">Top Rated</h2>
                  <!-- <div class="jtv-product">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img10.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img10.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div> -->
                  <?php
                    foreach($top_price as $top)
                    {
                  ?>
                  <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url('uploads/c2c/'.$top['feature_image'])?>" alt="" width="112.69px" height="112.69px"> <img class="secondary-img" src="front_assets/e_commerce/images/products/img08.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html"><?=$top['title']?></a></h3>
                        <div class="price-box">
                           <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?= number_format($top['price'],2)?> &#x9f3;</span> </p>

                           <!-- <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?=$top['price']?> </span> </p> -->
                        </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php
                    }
                  ?>

                  <!-- <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img08.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img09.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$225.00</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div> -->
               </div>
            </div>
            <div class="col-md-4 col-sm-6">
               <div class="jtv-single-cat">
                  <h2 class="cat-title">ON SALE</h2>
                  <div class="jtv-product">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img12.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img11.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box">
                           <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $99.00 </span> </p>
                           <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $119.00 </span> </p>
                        </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img05.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img10.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$189.00</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img01.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img03.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$88.99</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- service area start -->
            <div class="col-md-4 col-sm-6">
               <div class="jtv-single-cat">
                  <h2 class="cat-title">Fruit Market</h2>
                  <div class="jtv-product">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img15.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img10.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img03.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img08.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box">
                           <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                           <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                        </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="jtv-product jtv-cat-margin">
                     <div class="product-img"> <a href="single_product.html"> <img src="<?=base_url()?>front_assets/e_commerce/images/products/img09.jpg" alt=""> <img class="secondary-img" src="front_assets/e_commerce/images/products/img12.jpg" alt=""> </a> </div>
                     <div class="jtv-product-content">
                        <h3><a href="single_product.html">Lorem ipsum dolor sit amet</a></h3>
                        <div class="price-box"> <span class="regular-price"> <span class="price">$225.00</span> </span> </div>
                        <div class="jtv-product-action">
                           <div class="jtv-extra-link">
                              <div class="button-cart">
                                 <button><i class="fa fa-shopping-cart"></i></button>
                              </div>
                              <a href="#" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i></a> <a href="#"><i class="fa fa-heart"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- category-area end -->
   <!-- Footer Starts -->
   <?php $this->load->view('e_commerce/ecom_footer'); ?>
   <!-- Footer Ends -->
   <!-- Footer Link Starts -->
   <?php $this->load->view('e_commerce/ecom_footer_link'); ?>
   <!-- Footer Link Ends -->
   <script>
      function adToCart(id)
      {
        let qty = $('#qty').val();
        $.ajax({
         url:"<?php echo base_url(); ?>cart/add",
         method:"POST",
         data:{ad_id:id,qty:qty},
         success:function(data)
         {
           $.notify("added into your cart",{autoHide:true,clickToHide:false,className:'success',style:'bootstrap',position:'bottom left'});
           $(".notifyjs-container").attr("style", "width:350px");
           $('#cart_content').html(data);

         }
        });
       }


      //
        $('#cart_content').load("<?php echo base_url(); ?>cart/view");
      //


       $(document).on('click', '.remove-cart', function(){
        var rowid = $(this).attr("id");

        if(confirm("Are you sure you want to remove this?"))
        {
         $.ajax({
          url:"<?php echo base_url(); ?>cart/remove",
          method:"POST",
          data:{rowid:rowid},
          success:function(data)
          {
            $.notify("Product removed from Cart",{autoHide:true,clickToHide:false,className:'success',style:'bootstrap',position:'bottom left'});
             $(".notifyjs-container").attr("style", "width:350px");
           $('#cart_content').html(data);
          }
         });
        }
        else
        {
         return false;
        }
       });
      //
      //  $(document).on('click', '#clear_cart', function(){
      //   if(confirm("Are you sure you want to clear cart?"))
      //   {
      //    $.ajax({
      //     url:"<?php echo base_url(); ?>shopping_cart/clear",
      //     success:function(data)
      //     {
      //      alert("Your cart has been clear...");
      //      $('#cart_details').html(data);
      //     }
      //    });
      //   }
      //   else
      //   {
      //    return false;
      //   }
      //  });
      //
      // });
   </script>
