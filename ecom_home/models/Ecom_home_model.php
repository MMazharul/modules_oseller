<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecom_home_model extends CI_Model
{
    ////// Basic Model Function Starts /////
    public function insert($table_name,$data)
    {
       $this->db->insert($table_name, $data);
   }

   public function insert_ret($tablename, $tabledata)
   {
    $this->db->insert($tablename, $tabledata);
    return $this->db->insert_id();
}


public function update_function($columnName, $columnVal, $tableName, $data)
{
    $this->db->where($columnName, $columnVal);
    $this->db->update($tableName, $data);
}

public function update_function2($condition, $tableName, $data)
{
 $where = '( ' . $condition . ' )';
 $this->db->where($where);
 $this->db->update($tableName, $data);
}

public function delete_function_cond($tableName, $cond)
{
    $where = '( ' . $cond . ' )';
    $this->db->where($where);
    $this->db->delete($tableName);
}
public function delete_function($tableName, $columnName, $columnVal)
{
    $this->db->where($columnName, $columnVal);
    $this->db->delete($tableName);
}

public function select_all($table_name)
{
 $this->db->select('*');
 $this->db->from($table_name);
 $query = $this->db->get();
 $result = $query->result_array();
 return $result;
}

public function only_select_data($selector,$table_name)
{
  $this->db->select($selector);
  $this->db->from($table_name);
  $query = $this->db->get();
  $result = $query->result_array();
  return $result;
}



public function select_all_name_ascending($col_name,$table_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by($col_name,'ASC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_all_decending($table_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by('created_at','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_with_where($selector, $condition, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result = $this->db->get();
    return $result->result_array();
}

public function select_with_where_decending($selector, $condition, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','DESC');
    $result = $this->db->get();
    return $result->result_array();
}

public function select_with_decending_limit($selector, $tablename,$limit_value)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    // $where = '(' . $condition . ')';
    // $this->db->where($where);
    $this->db->order_by('id','DESC');
    $result = $this->db->get();
    return $result->result_array();
}

public function select_with_group_by_decending($selector, $tablename, $col_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $this->db->group_by($col_name);
    $this->db->order_by('created_at','DESC');
    $result = $this->db->get();
    return $result->result_array();
}

public function select_with_group_by($selector, $tablename, $col_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $this->db->group_by($col_name);
    $result = $this->db->get();
    return $result->result_array();
}

public function select_with_group_by_where($selector, $tablename, $col_name,$condition,$group_by)
{
 $this->db->select($selector);
 $this->db->from($tablename);
 $this->db->group_by($col_name);
 $where = '(' . $condition . ')';
 $this->db->where($where);
 $result = $this->db->get();
 return $result->result_array();
}

public function select_order_with_where($selector, $condition, $tablename,$order_name,$order_by)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_name,$order_by);
    $result = $this->db->get();
    return $result->result_array();
}



public function count_with_where($selector, $condition, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result = $this->db->get();
    return $result->num_rows();
}

public function sum_with_where($selector, $condition,$col_name, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result = $this->db->get();
    return $result->select_sum($col_name);
}

public function select_join($selector,$table_name,$join_table,$join_condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $result=$this->db->get();
    return $result->result_array();
}


public function banner_list($condition='')
{
    $this->db->select('*,s.id as banner_id');
    $this->db->from('b2b_sidebanner s');
    $this->db->join('category','category.id=s.category_id','LEFT');
    $this->db->join('subcategory','subcategory.id=s.subcategory_id','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result=$this->db->get();
    return $result->result_array();
}


public function ads_list($condition='')
{
    $this->db->select('*,ads.status as a_status,ads.id as a_id');
    $this->db->from('ads');
    $this->db->join('service','service.id=ads.service_id','LEFT');
    $this->db->join('category','category.id=ads.category_id','LEFT');
    $this->db->join('subcategory','subcategory.id=ads.subcategory_id','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('ads.id','DESC');
    $result=$this->db->get();
    return $result->result_array();
}

public function reported_ads_list($condition='')
{
    $this->db->select('*,ads_report.created_at as report_time,ads_report.id as report_id, r2.name as admin_name,r2.image as admin_image,registration.name as reporter_name,registration.image as reporter_image');
    $this->db->from('ads_report');
    $this->db->join('ads','ads.id=ads_report.ads_id','LEFT');
    $this->db->join('registration','registration.login_id=ads_report.reported_by','LEFT');
    $this->db->join('service','service.id=ads.service_id','LEFT');
    $this->db->join('category','category.id=ads.category_id','LEFT');
    $this->db->join('subcategory','subcategory.id=ads.subcategory_id','LEFT');
    $this->db->join('registration r2','r2.login_id=ads_report.replied_by','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('report_id','DESC');
    $result=$this->db->get();
    return $result->result_array();
}



public function select_left_join($selector,$table_name,$join_table,$join_condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $result=$this->db->get();
    return $result->result_array();
}

public function select_left_join_two($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}



public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_group_by($selector,$table_name,$join_table,$join_condition,$condition,$col_name)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->group_by($col_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_join_two($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $this->db->join($join_table1,$join_condition1);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_left_join_two($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_two_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$condition,$col_name)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->group_by($col_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_three($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_join_three($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $this->db->join($join_table1,$join_condition1);
    $this->db->join($join_table2,$join_condition2);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_four_distinct($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $this->db->distinct();
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_four($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_five($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_five_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition,$col_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->group_by($col_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_six($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_six_sum($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition,$col_name,$sum)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    $result->$this->db->select_sum('$col_name','$sum');
    return $result->result_array();
}

public function select_where_left_join_six_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition,$col_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $this->db->group_by($col_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join_seven($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $this->db->join($join_table6,$join_condition6,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

    // public function select_where_left_join_eight($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$join_table7,$join_condition7,$condition)
    // {
    //     $this->db->select($selector);
    //     $this->db->from($table_name);
    //     $this->db->join($join_table,$join_condition,'left');
    //     $this->db->join($join_table1,$join_condition1,'left');
    //     $this->db->join($join_table2,$join_condition2,'left');
    //     $this->db->join($join_table3,$join_condition3,'left');
    //     $this->db->join($join_table4,$join_condition4,'left');
    //     $this->db->join($join_table5,$join_condition5,'left');
    //     $this->db->join($join_table6,$join_condition6,'left');
    //     $this->db->join($join_table7,$join_condition7,'left');
    //     $where = '(' . $condition . ')';
    //     $this->db->where($where);
    //     //$this->db->order_by($order_col,$order_action);
    //     $result=$this->db->get();
    //     return $result->result_array();
    // }


public function select_where_left_join_ten($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$join_table7,$join_condition7,$join_table8,$join_condition8,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $this->db->join($join_table6,$join_condition6,'left');
    $this->db->join($join_table7,$join_condition7,'left');
    $this->db->join($join_table8,$join_condition8,'left');

    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_left_join_seven_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$condition,$col_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $this->db->join($join_table1,$join_condition1,'left');
    $this->db->join($join_table2,$join_condition2,'left');
    $this->db->join($join_table3,$join_condition3,'left');
    $this->db->join($join_table4,$join_condition4,'left');
    $this->db->join($join_table5,$join_condition5,'left');
    $this->db->join($join_table6,$join_condition6,'left');
    $this->db->group_by($col_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}




public function select_where_two_join($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $this->db->join($join_table2,$join_condition2);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_join_order_by($selector,$table_name,$join_table,$join_condition,$condition,$order_col,$order_action)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function get_blog($condition,$order_col,$order_action)
{
    //$this->db->select('*,blog.id as b_id,blog.created_at as blog_date,blog.image as b_image,blog.status as b_status,(select count(blog_comment.id) from blog_comment where blog_comment.status=1 and b_id=blog_comment.blog_id) as total_comment,(select GROUP_CONCAT(DISTINCT c.category_name) from blog b,category c where b_id=b.id and c.id in (blog.cat_id)) as cat_names');

    $query="SELECT  *,blog.id as b_id,blog.created_at as blog_date,blog.image as b_image,blog.status as b_status,(select count(blog_comment.id) from blog_comment where blog_comment.status=1 and b_id=blog_comment.blog_id) as total_comment, GROUP_CONCAT(c.category_name ORDER BY c.id) cat_names FROM blog INNER JOIN category c ON FIND_IN_SET(c.id, blog.cat_id) > 0 inner join registration on registration.login_id=blog.login_id where (".$condition.") GROUP BY blog.id";

    //$this->db->from('blog');
    //$this->db->join('registration','registration.login_id=blog.login_id');
    // $this->db->join('category','category.id=blog.cat_id');
    //$where = '(' . $condition . ')';
    //$this->db->where($where);

    //$this->db->order_by($order_col,$order_action);
    //$result=$this->db->get();
    $result = $this->db->query($query) or die ("Schema Query Failed");
    return $result->result_array();
}




public function get_blog_comment($condition,$order_col,$order_action,$limit)
{
    $this->db->select('*,blog.id as b_id,blog_comment.created_at as com_date');
    $this->db->from('blog_comment');
    $this->db->join('blog','blog.id=blog_comment.blog_id');
    $this->db->join('registration','registration.login_id=blog_comment.login_id');
    $this->db->join('category','category.id=blog.cat_id');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_col,$order_action);
    $this->db->limit($limit);
    $result=$this->db->get();
    return $result->result_array();
}



    ////// Basic Model Function End ///////


public function exist_email($email)
{
    $this->db->select('email');
    $this->db->from('login');
    $this->db->where('email',$email);
    $result=$this->db->get();
    return $result->result_array();
}

public function get_last_product_code()
{
 $this->db->select('p_code');
 $this->db->from('product');
 $this->db->order_by('p_id',"desc");
 $this->db->limit(1);
 $query = $this->db->get();
 $result = $query->result_array();
 return $result;
}

public function get_last_sell_code()
{
    $this->db->select('sell_code');
    $this->db->from('sell');
    $this->db->order_by('sell_id',"desc");
    $this->db->limit(1);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function get_last_buy_code()
{
    $this->db->select('buy_code');
    $this->db->from('buy');
    $this->db->order_by('buy_id',"desc");
    $this->db->limit(1);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function get_product_details($p_id)
{
 $this->db->select('*');
 $this->db->from('product');
 $this->db->where('p_id',$p_id);
 $query = $this->db->get();
 $result = $query->result_array();
 return $result;
}


public function columns($database, $table)
{
        //$query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '$table'AND table_schema = '$database'";
    $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = '$table'
    AND table_schema = '$database'";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;
}


	///  For Union Two table
function get_merged_result()
{
    $this->db->select("name,id,phone_second as web");
       // $this->db->distinct();
    $this->db->from("registration");
        //$this->db->where_in("id",$model_ids);
    $this->db->get();
    $query1 = $this->db->last_query();

    $this->db->select("name,id,website as web");
       // $this->db->distinct();
    $this->db->from("company");
       // $this->db->where_in("id",$model_ids);

    $this->db->get();
    $query2 =  $this->db->last_query();
    $query = $this->db->query($query1." UNION ".$query2);

    return $query->result_array();
}


public function shout_details_all()
{
    $query = "SELECT shout.*,login.*,shout.id as sh_id,shout.type,concat(login.country_code,login.mobile_no) as phone,
    service.service_name, category.category_name as cat_name, subcategory.subcategory_name as subcat_name,
    divisions.name as div_name, districts.name as dis_name, area.area_title as area_name FROM shout
    LEFT JOIN login on login.id = shout.user_id
    LEFT JOIN service on service.id = shout.service_id
    LEFT join category on category.id = shout.category_id LEFT JOIN subcategory on shout.subcategory_id = subcategory.id
    LEFT JOIN divisions on divisions.id = shout.div_id LEFT JOIN districts on districts.id = shout.dis_id LEFT JOIN area
    on area.area_id = shout.area_id where shout.status IN (1,2) order by sh_id desc

    "

    ;


    $result = $this->db->query($query) or die ("Schema Query Failed");


    $result=$result->result_array();
    return $result;
}

public function get_all_item_type_list($value='')
{
 $query ="SELECT  i.*,s.subcategory_name,
 GROUP_CONCAT(e.brand_name ORDER BY e.id)  as brand_name
 FROM   item_type_extra i
 LEFT JOIN subcategory s
 on s.id=i.subcategory_id
 LEFT JOIN ecom_brands e
 ON FIND_IN_SET(e.id, i.brand_id)
 where i.status in(1,3)
 GROUP   BY i.id order by i.id desc";
 $result = $this->db->query($query) or die ("Schema Query Failed");


 $result=$result->result_array();
 return $result;

}

public function get_all_item_type_list_id($id='')
{
 $query ="SELECT  i.*,s.subcategory_name,
 GROUP_CONCAT(e.brand_name ORDER BY e.id)  as brand_name
 FROM   item_type_extra i
 LEFT JOIN subcategory s
 on s.id=i.subcategory_id
 LEFT JOIN ecom_brands e
 ON FIND_IN_SET(e.id, i.brand_id)
 where i.status in(1,3) and i.id=$id
 GROUP   BY i.id order by i.id desc";
 $result = $this->db->query($query) or die ("Schema Query Failed");


 $result=$result->result_array();
 return $result;

}


public function get_all_group_deal_info($value='')
{
    $query ="SELECT e.id, e.*,r.name, GROUP_CONCAT(a.title) as ad_title
    FROM
    ads a LEFT JOIN  ecom_deals e
    ON FIND_IN_SET(a.id, e.ad_id)
    LEFT join registration r on r.login_id=a.login_id
    where e.deal_type=3 and e.status=1
    GROUP BY e.id
    ";
    $result = $this->db->query($query) or die ("Schema Query Failed");


    $result=$result->result_array();
    return $result;

}

public function get_all_group_deal_info_id($id='')
{
    $query ="SELECT e.id, e.*,r.name, GROUP_CONCAT(a.title) as ad_title
    FROM
    ads a LEFT JOIN  ecom_deals e
    ON FIND_IN_SET(a.id, e.ad_id)
    LEFT join registration r on r.login_id=a.login_id
    where e.deal_type=3 and e.status=1 and e.id=$id
    GROUP BY e.id
    ";
    $result = $this->db->query($query) or die ("Schema Query Failed");


    $result=$result->result_array();
    return $result;

}



}
?>
