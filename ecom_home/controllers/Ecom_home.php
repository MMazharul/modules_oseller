<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecom_home extends MX_Controller
{

  function __construct()
  {
    date_default_timezone_set('Asia/Dhaka');
    $this->load->library('cart');

      // $this->load->model('admin_model');
    $this->load->model('home/home_model');
    $this->load->model('blog/blog_model');
    $this->load->model('admin/admin_model');
    $this->load->model('ecom_home_model');

    if($this->session->userdata('language_select')=='bangla')
    {
      $this->lang->load('admin', 'bangla');
    }
    else
    {
      $this->lang->load('admin', 'english');
    }

  }

    public function index()
    {
      $data['active']='Home';
      $data['page_title']=$this->lang->line('dashboard_label');
      $data['sliders']=$this->ecom_home_model->select_with_where('*','status=1','	ecom_home_slider');
      $data['products']=$this->admin_model->ads_list('ads.status=1 and is_showin_ecom=1');

      $data['blogs']=$this->admin_model->select_with_where('*','blog.status=1','blog');
      $data['top_price']=$this->admin_model->select_order_with_where_limit('feature_image,title,price','status=1','ads','price','DESC',3);

      $data['category_info']=$this->ecom_home_model->select_with_where('*','status=1 and is_to_show_in_ecom=1','category');
      // echo "<pre>";
      // echo json_encode($data['category_info']);
      // die;
      $data['subcategory_info']=$this->ecom_home_model->select_with_where('*','status=1','subcategory');

      $data['item_info']=$this->ecom_home_model->select_with_where('*','status=1','item_type_extra');

      $cat_id=array();
      $subcat_id=array();
      foreach ($data['category_info'] as $key => $ro) {
        $count=0;
        foreach ($data['subcategory_info'] as $key1 => $value) {

          if($ro['id']==$value['category_id']){
           $count=1;
         }

       }
       if($count==0) {

        $cat_id[]=$ro['id'];
        }
    }

    foreach ($data['subcategory_info'] as $key => $ro) {
      $count=0;
      foreach ($data['item_info'] as $key1 => $value) {

        if($ro['id']==$value['subcategory_id']){
         $count=1;
       }

     }
     if($count==0) {

      $sub_cat_id[]=$ro['id'];

    }

  }

  $data['subcat_id'] = $sub_cat_id;
  $data['cat_id'] = $cat_id;
  // echo "<pre>";
  // print_r($data);
  // echo "<pre>";
  // die;

  $this->load->view('index',$data);
  }
  public function cart_list()
  {
    $this->load->view('cart_list');
  }


    public function update_quenty()
    {
      $this->load->library('cart');
      $rowid=$this->input->post('rowid');
      $qunty=$this->input->post('qty');

      //$qunty+=$qunty;

      $data = array(
      'rowid' => $rowid,
      'qty' => $qunty
      );

      $this->cart->update($data);

      echo $this->cart_view();
    }

  public function cart_view()
  {
    $output = '';
  $output .= '
  <table class="table table-bordered cart_summary">
    <thead>
      <tr>
       <th style="text-align:center">Product</th>
       <th style="text-align:center">Name</th>
       <th style="text-align:center">Quantity</th>
       <th style="text-align:center">price</th>
       <th style="text-align:center">Total</th>
       <th style="text-align:center" class="action"><i class="fa fa-trash-o"></i></th>
      </tr>
  </thead>

  ';
  $count = 0;
  foreach($this->cart->contents() as $items)
  {
   $count++;
   $output .= '
   <tr>
    <td class="cart_product"><a href="#"><img src="'.base_url('uploads/c2c/'.$items['image']).'" alt="Product"></a></td>
    <td >'.$items["name"].'</td>
    <td class="qty"><input type="text" min="" class="form-control input-sm" id="up" data-id='.$items["rowid"].' value="'.$items["qty"].'"></td>
    <td class="price">'.number_format($items["price"],2).'&#x9f3</td>
    <td class="price">'.number_format($items["subtotal"],2).'&#x9f3</td>
    <td class="action"><a href="#"><i class="icon-close"></i></a></td>
   </tr>

   ';
  }
  $output .= '
  <tfoot>
  <tr>
    <td colspan="4"><strong>Total</strong></td>
    <td colspan="2"><strong>'.number_format($this->cart->total(),2).'&#x9f3 </strong></td>
  </tr>
  <tfoot>

  </table>
  ';

  if($count == 0)
  {
   $output = '<h3 align="center">Cart is Empty</h3>';
  }
  echo $output;
  }

  // blog post
  public function singleBlog()
  {
     $blogId = $this->uri->segment(3);

     $data['singleBlogInfo'] = $this->ecom_home_model->select_where_join_two('blog.title,blog.image,blog.description,blog.created_at,login.username,category.category_name','blog','category','category.id = blog.cat_id','login','login.id = blog.login_id','blog.id='.$blogId);
     $data['blogs'] = $this->ecom_home_model->select_with_where('id,title,image,created_at','status=1','blog');

    $this->load->view('blog/single_blog',$data);
  }

  public function singleProduct()
 {
   $ad_id = $this->uri->segment(3);
   $data['exist_fav']=0;
   if($this->session->userdata('login_id'))
   {
       $data['exist_fav']=count($this->ecom_home_model->select_with_where('*', 'login_id='.$this->session->userdata('login_id').' AND find_in_set("'.$ad_id.'",is_favourite_ecom)', 'registration'));
   }

   $data['singleProduct'] = $this->ecom_home_model->select_where_join('ads.*,category.category_name','ads','category','category.id=ads.category_id','ads.id='.$ad_id);
   $data['singleProduct'] = $data['singleProduct'][0];
   $data['productReviews'] = $this->ecom_home_model->select_where_join('ecom_reviews.*,login.username','ecom_reviews','login','login.id = ecom_reviews.user_id','ecom_reviews.ad_id='.$data['singleProduct']['id']);
   // $data['categoryProducts'] = $this->ecom_home_model->select_with_where('ads.title_link,ads.feature_image,ads.price','category_id='.$data['singleProduct']['category_id'].'AND is_showin_ecom=1','ads');
   
   $this->load->view('product/single_product',$data);
 }

  public function add_favourite()
 {
     $login_id=$this->session->userdata('login_id');
     //$data['is_favourite']=array();
     $a_id=$this->input->post('ad_id');

     $data['is_favourite_ecom']='';

     $pre_ads_id=$this->ecom_home_model->select_with_where('*', 'login_id='.$login_id, 'registration');


     $exist_fav=$this->ecom_home_model->select_with_where('*', 'login_id='.$login_id.' AND find_in_set("'.$a_id.'",is_favourite_ecom)', 'registration');

     if(count($exist_fav)>0)
     {
         $fav_explode=explode(',',$pre_ads_id[0]['is_favourite_ecom']);

         foreach ($fav_explode as $ads_id)
         {
             if($ads_id==$a_id)
             {

             }
             else
             {
                 $data['is_favourite_ecom'].=$ads_id.',';
             }
         }

         $data['is_favourite_ecom']=substr($data['is_favourite_ecom'],0,-1);
         echo 0;
     }

     else
     {
         if($pre_ads_id[0]['is_favourite_ecom']=='')
         {
             $data['is_favourite_ecom']=$a_id;
         }
         else
         {
             $data['is_favourite_ecom']=$pre_ads_id[0]['is_favourite_ecom'].','.$a_id;
         }

         echo 1;
     }

     if($data['is_favourite_ecom']==0)
     {
         $data['is_favourite_ecom']='';
     }

     $this->ecom_home_model->update_function('login_id',$login_id,'registration',$data);
 }

 public function checkout()
 {
   // $divisions =
   $login_id = $this->session->userdata('login_id');
   if($login_id)
   {
     $data['divisions'] = $this->ecom_home_model->only_select_data('id,name,bn_name','divisions');
     $this->load->view('checkout',$data);
   }
   else
   {
     $currentURL = current_url();
     $this->session->set_userdata('seturl',$currentURL);
     $setUrl = $this->session->userdata('seturl');
     redirect('login','refresh');
   }


  }

  public function checkOutStore()
  {
     $this->load->library('cart');
    $shippingData = $this->shippingPostInfo($_POST);
    $billingData = $this->billingPostInfo($_POST);
    $shippingId = $this->ecom_home_model->insert_ret('shipping_address',$shippingData);
    $billingId = $this->ecom_home_model->insert_ret('billing_address',$billingData);
     $invoiceId = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
    $order['invoice_id'] = $invoiceId;
    $order['login_id'] = $this->session->userdata('login_id');
    $order['grand_total'] = $this->cart->total();
    $order['discount'] = 'fixed';
    $order['status'] = 0;
    $order['order_date'] = date('Y/m/d');
    $order['shipping_id'] = $shippingId;
    $order['billing_id'] = $billingId;
    $order['payment_method'] =$this->input->post('payment_method');
    $order['create_at'] = date('Y/m/d');
    $order['update_at'] = date('Y/m/d');
    $order_id = $this->ecom_home_model->insert_ret('orders',$order);
    $this->cartInfo($order_id);
    $this->cart->destroy();
    redirect('order/payment/'.$invoiceId);
  }

  public function payment($invoiceId='')
  {
    $data['invoiceId'] = $invoiceId;

     $data['userInfo'] = $this->ecom_home_model->select_where_join_two('orders.id as order_id,orders.order_date,orders.grand_total,shipping_address.*,shipping_address.id as ship_id,billing_address.*,billing_address.id as bill_id','orders','shipping_address','shipping_address.id=orders.shipping_id','billing_address','billing_address.id=orders.billing_id','orders.invoice_id='.$invoiceId);
     $order_id = $data['userInfo'][0]['order_id'];
     $data['userInfo'] = $data['userInfo'][0];

     $data['orders'] = $this->ecom_home_model->select_with_where('*','order_id='.$order_id,'os_order_details');
    $this->load->view('payment',$data);

  }


  public function shippingPostInfo($info)
  {
    $division = $this->ecom_home_model->select_with_where('name','id='.$info['shipping_division'],'divisions');
    $district = $this->ecom_home_model->select_with_where('name','id='.$info['shipping_district'],'districts');
    $data['shipping_name'] = $info['shipping_name'];
    $data['shipping_phone'] = $info['shipping_phone'];
    $data['shipping_alternate_phone']=$info['shipping_alternate_phone'];
    $data['shipping_division'] = $division[0]['name'];
    $data['shipping_district'] = $district[0]['name'];
    $data['shipping_area'] = $info['shipping_area'];
    $data['shipping_email'] = $info['shipping_email'];
    $data['shipping_address'] = $info['shipping_address'];
    return $data;
  }

  public function billingPostInfo($info)
  {
    $division = $this->ecom_home_model->select_with_where('name','id='.$info['billing_division'],'divisions');
    $district = $this->ecom_home_model->select_with_where('name','id='.$info['billing_district'],'districts');

    $data['billing_name'] = $info['billing_name'];
    $data['billing_phone'] = $info['billing_phone'];
    $data['billing_alternate_phone'] = $info['billing_alternate_phone'];
    $data['billing_division'] = $division[0]['name'];
    $data['billing_district'] = $district[0]['name'];
    $data['billing_area'] = $info['billing_area'];
    $data['billing_email'] = $info['billing_email'];
    $data['billing_address'] = $info['billing_address'];
    return $data;
  }

  public function cartInfo($order_id)
  {

    $carts = $this->cart->contents();
    foreach($carts as $cart)
    {
      $orderItem['order_id'] = $order_id;
      $orderItem['p_id'] = $cart['id'];
      $orderItem['price'] = $cart['price'];
      $orderItem['qty'] = $cart['qty'];
      $orderItem['subtotal'] = $cart['subtotal'];
      $this->ecom_home_model->insert('os_order_details',$orderItem);

    }

  }
  public function productReviewPost()
  {
    $this->session->unset_userdata('seturl');
    $login_id = $this->session->userdata('login_id');
    if($login_id)
    {
      $data['ad_id'] = $this->input->post('ad_id');
      $data['user_id'] = $login_id;
      $data['review'] = $this->input->post('review');
      $data['rating'] = $this->input->post('rating');
      $data['created_at'] = date('Y-m-d');
      $this->ecom_home_model->insert('ecom_reviews',$data);
      $this->session->set_userdata('success','Thank you for your review');
      redirect($_SERVER['HTTP_REFERER']);
    }
    else {
      $this->session->set_userdata('seturl',$_POST['url']);
      redirect('login','refresh');
    }

  }


}
