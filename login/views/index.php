<?php
   $this->load->view('front/header_link');
   ?>
<body class="form-bg scroll">
   <?php
      $this->load->view('front/header');
      ?>
   <style>
   </style>
   <div class="">
      <div class="container">
         <div class="col-md-7 mx-auto border m-5 bg-white" style="border-radius: 10px;">
            <form method="POST" action="login/login_check" class="login-form">
               <h5 class="text-center pb-3 font-weight-bold"><?=$this->lang->line('login_title_label')?></h5>
               <div class="container">
                  <div class="row">
                     <?php $this->load->view('front/login_sc');?>
                  </div>
               </div>
               <div class="row mt-4 mb-4">
                  <div class="col-md-5">
                     <hr style="border-top: 1px solid #ccc">
                  </div>
                  <div class="col-md-2 text-center font-weight-bold" style="line-height: 2;"><span><?=$this->lang->line('login_or_label')?></span></div>
                  <div class="col-md-5">
                     <hr style="border-top: 1px solid #ccc">
                  </div>
               </div>
               <div class="col-md-4 login-avatar">
                  <img src="front_assets/images/login-img-min.png" alt="">
               </div>
               <?php if($this->session->userdata('log_err')){ ?>
               <div class="alert alert-danger">
                  <button class="close" data-close="alert"></button>
                  <span><?=$this->session->userdata('log_err');?></span>
               </div>
               <?php } $this->session->unset_userdata('log_err');?>
               <?php if($this->session->userdata('log_ssc')){ ?>
               <div class="alert alert-success">
                  <button class="close" data-close="alert"></button>
                  <span><?=$this->session->userdata('log_ssc');?></span>
               </div>
               <?php } $this->session->unset_userdata('log_ssc');?>
               <input type="email" name="email" class="form-control rounded-2 mb-2" placeholder="<?=$this->lang->line('login_email_label')?>">

               <div>
                  <input autocomplete="new-password"accept=" " type="password"  class="form-control rounded-2 mb-2" id="password" name="password" placeholder="<?=$this->lang->line('login_password_label')?>">
                  <span toggle="#password" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
               </div>
               <div class="row">
                  <div class="col-md-4">
                    <button id="submit" type="submit" class="btn btn-primary border-0 rounded-0"><?=$this->lang->line('login_submit_label')?></button>
                  </div>
                  <div class="col-md-8">
                     <span class="float-right"><a href="forget"><?=$this->lang->line('login_forgetp_label')?>?</a></span>
                     <?=$this->lang->line('login_register_label')?>
                  </div>
               </div>

            </form>
         </div>
      </div>
   </div>
   <?php
      $this->load->view('front/footer');
      ?>
   <?php
      $this->load->view('front/footer_link');
      ?>
   <script>
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
   </script>

   <script>
      $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye-slash fa-eye");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
      });
   </script>
</body>
</html>
