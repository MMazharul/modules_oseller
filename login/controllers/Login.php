<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {


    function __construct()
    {
        $this->load->model('login_model');
        $this->load->model('search/search_model');
        $this->load->model('home/home_model');
        $this->load->library('facebook');

        date_default_timezone_set('Asia/Dhaka');
        if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('front', 'bangla');
            $this->lang->load('login', 'bangla');
        }
        else
        {
            $this->lang->load('front', 'english');
            $this->lang->load('login', 'english');
        }

    }
    public function index()
    {
      require_once APPPATH . "modules\google_login\libraries\google-api-php-client-master\Google\autoload.php";
      require_once APPPATH . "modules\google_login\libraries\google-api-php-client-master\Google\Auth\OAuth2.php";

      //
      //  // Create Client Request to access Google API
      // $client = new Google_Client();
      // $client->setApplicationName("osellers");
      // $client->setClientId('318808742092-q9ktsbbk4qcfic8qgheo7b9qufusv8th.apps.googleusercontent.com');
      // $client->setClientSecret('uExeIYn8wsb2SQ1JNfwnLTpx');
      // $client->setRedirectUri('http://localhost/codeigniter-blog/index.php/Social_auth/signup_with_google');
      // $client->setDeveloperKey('');
      // $client->addScope("https://www.googleapis.com/auth/userinfo.email");

      // login token
      $loginTkn = new Google_Client();
      $loginTkn->setApplicationName("osellers");
      $loginTkn->setClientId('318808742092-q9ktsbbk4qcfic8qgheo7b9qufusv8th.apps.googleusercontent.com');
      $loginTkn->setClientSecret('uExeIYn8wsb2SQ1JNfwnLTpx');
      $loginTkn->setRedirectUri('https://osellers.dev/google_login');
      $loginTkn->setDeveloperKey('');
      $loginTkn->addScope("https://www.googleapis.com/auth/userinfo.email");
       // link
       // $authUrl = $client->createAuthUrl();
       // $data['authUrl'] = $authUrl;

      $loginUrl = $loginTkn->createAuthUrl();
      $data['loginUrl'] = $loginUrl;

      if($this->session->userdata('language_select')=='bangla')
      {
        $data['pageTitle']='লগ-ইন';
      }
      else
      {
        $data['pageTitle']='Log-in';
      }
        $data['nav_active']='login';
        $data['company_info']=$this->home_model->select_all('company');
        $this->load->view('index',$data);
    }

    public function login_check()
    {
        $data['company_info']=$this->home_model->select_all('company');

        $email = $this->input->post('email');

        $password = $this->encryptIt($this->input->post('password'));

        $res = $this->login_model->check_login($email, $password);
        if (count($res) > 0)
        {

            $this->session->set_userdata('login_id', $res[0]['id']);
            $this->session->set_userdata('user_type' ,$res[0]['type']);
            $this->session->set_userdata('email' ,$res[0]['email']);
            $this->session->set_userdata('phone' ,$res[0]['mobile_no']);

            $name=$this->login_model->get_name($res[0]['id']);
            $this->session->set_userdata('name',$name[0]['name']);
            $this->session->set_userdata('image',$name[0]['image']);

            $this->session->set_userdata('login_scc','Welcome to your  Dashboard.');

            if($res[0]['type']==0)
            {
                redirect('account','refresh');
            }
            else if($res[0]['type']==1)
            {
              if($this->session->userdata('seturl'))
              {
                redirect($this->session->userdata('seturl'),'refresh');
              }
              else {
                redirect('user','refresh');
              }

            }
            else
            {
                $this->session->set_userdata('log_err','You are not admin.');
                redirect('login', 'refresh');
            }
        }


        else
        {
            $this->session->set_userdata('log_err','Email or Password is Incorrect.');
            redirect('login', 'refresh');
        }

    }


    public function ajax_login_check($value='')
    {
        $email = $this->input->post('username');
        $password = $this->encryptIt($this->input->post('password'));

        $res = $this->login_model->check_login($email, $password);
        if (count($res) > 0)
        {
            $this->session->set_userdata('login_id', $res[0]['id']);

            $this->session->set_userdata('user_type' ,$res[0]['type']);
            $this->session->set_userdata('email' ,$res[0]['email']);
            $this->session->set_userdata('phone' ,$res[0]['mobile_no']);

            $name=$this->login_model->get_name($res[0]['id']);

            $this->session->set_userdata('name',$name[0]['name']);
            $this->session->set_userdata('image',$name[0]['image']);

            if($res[0]['type']==1)
            {
                echo 1;
            }
        }

        else
        {
            echo 0;
        }

    }

    function encryptIt($string)
    {


        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }








}
