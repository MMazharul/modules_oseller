<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
	
	 public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function get_name($id)
    {
        $this->db->select('*');
        $this->db->from('registration');
        $this->db->where('login_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }
}