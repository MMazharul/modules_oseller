<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 12:51 PM
 */
?>

@extends('admin.layout.master')

@section('body')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">

        <div class="card-body">

            <form role="form" method="get" action="{{ route('bus-pasengerinfo') }}">


                <div class="col-md-3">
                    <h5>Bus Number</h5>
                    <div class="input-group">
                        <select class="select2 form-control" name="bus_id">
                            <option></option>
                            @foreach($all_bus_reg as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>

                <div class="row form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </div>

            </form>

        </div>


        <div class="card-body">
            <div>
                <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Passenger Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Seat No</th>
                    <th>Balance</th>
                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                <!-- @php
                    $total_fare = 0;

                @endphp -->

                @foreach($passengers as $k=>$data)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $data->name}}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->phone }}</td>
                        <td>{{ $data->seat_number }}</td>
                        <td>{{ $data->total_fare }}</td>

                    </tr>

                    @php
                        $total_fare += $data->total_fare;

                    @endphp
                    @if($loop->last)
                        <tr>
                            <th colspan="5"> Total </th>
                            <th >{{ create_money_format($total_fare) }}</th>
                        </tr>
                    @endif
                @endforeach


                </tbody>
            </table>

        </div>
    </div>


@endsection
@section('script')


    <script src="{{ asset('public/assets/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $( ".datepicker" ).datepicker();

            $('#btn-print, .btn-print').printPage();
        });
    </script>

@endsection
