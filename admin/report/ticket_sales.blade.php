<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 12:51 PM
 */
?>

@extends('admin.layout.master')

@section('body')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">

        <div class="card-body">

            <form role="form" method="get" action="{{ route('report.ticket_sale') }}">

                <div class="row">
                    <div class="col-md-3">
                        <h5> Point Name</h5>
                        <div class="input-group">
                           <select class="select2 form-control" name="point_id">
                               <option></option>
                               @foreach($agents as $key=>$value)
                                   <option value="{{$key}}">{{$value}}</option>
                                   @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h5>Route Name</h5>
                        <div class="input-group">
                            <select class="select2 form-control" name="route_id">
                                <option></option>
                                @foreach($trip_routes as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h5>From Date</h5>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker" value="" name="start_date" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h5>To Date</h5>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker" value="" name="end_date" autocomplete="off">
                        </div>
                    </div>

                </div>
                <br>

                <div class="row form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </div>

            </form>

        </div>


        <div class="card-body">
            <div>
                <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Route Name</th>
                    <th>Agent Name</th>
                    <th>Balance</th>
                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                @php
                    $total_fare = 0;
                    $total_commission = 0;
                @endphp

                @foreach($ticket_sales as $k=>$data)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $data->tripRoute->name }}</td>
                        <td>{{ $data->agent->full_name }}</td>
                        <td>{{ $data->total_fare }}</td>
                        <!-- <td>{{ $data->commission }}</td> -->
                    </tr>
                    @php
                        $total_fare += $data->total_fare;
                        
                    @endphp
                        @if($loop->last)
                        <tr>
                            <th colspan="3"> Total </th>
                            <td>{{ create_money_format($total_fare) }}</td>
                            <!-- <td>{{ create_money_format($total_commission) }}</td> -->
                        </tr>
                        @endif
                @endforeach

                </tbody>
            </table>

        </div>
    </div>


@endsection
@section('script')


    <script src="{{ asset('public/assets/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $( ".datepicker" ).datepicker();

            $('#btn-print, .btn-print').printPage();
        });
    </script>

@endsection
