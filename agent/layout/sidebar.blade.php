
<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">

        <li class="@if(request()->is('agent/dashboard')) @endif"><a href="{{route('agent.dashboard')}}"><i class="fa fa-fw fa-tachometer-alt"></i> Dashboard</a></li>
        <li class="@if(request()->is('agent/trip-manage')) active @endif"><a href="{{route('agent.tripFind')}}"><i class="fa-fw  fa fa-road"></i> Trip Search</a></li>
        <li class="@if(request()->is('agent/ticket-log')) active @endif"><a href="{{route('agent.ticketlog')}}"><i class="fa-fw  fa fa-road"></i> Ticket Log</a></li>
        <li class="@if(request()->is('agent/deposit')) active @endif"><a href="{{route('deposit')}}"> <i class="fas fa-dollar-sign"></i> Deposit</a></li>

        <li>
            <a href="#sm_report" data-toggle="collapse">
                <i class="fa fa-fw fa-money-bill-alt"></i> Reports
            </a>
            <ul id="sm_report" class="list-unstyled collapse @if(request()->path() == 'agent/report') show
                @elseif(request()->path() == 'agent/report/agent-ticket-sales') show
                @endif ">

                <li class="@if(request()->path() == 'agent/report/agent-ticket-sales') active @endif"><a href="{{route('report.agent_ticket_sale')}}"><i class="icon fa fa-money-bill-alt"></i> Ticket Sale</a></li>
            </ul>
        </li>

        <li class="@if(request()->is('agent/deposit-log')) active @endif"><a href="{{route('agent.depositLog')}}"> <i class="fa fa-exchange" aria-hidden="true"></i>
                Deposit Log</a></li>
        <li class="@if(request()->is('agent/transaction-log')) active @endif"><a href="{{route('agent.trx')}}"> <i class="fas fa-exchange"></i> Transaction Log</a></li>

    </ul>
</div>
