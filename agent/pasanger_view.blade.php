@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop

@section('body')
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">

        <div class="card-body">

            <div>
                <a class="btn btn-sm  btn-primary pull-right" href="{{route('report.agent_ticket_sale')}}" id="btn-print"> <i class="fa fa-backward"></i> Back </a>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>

                    <th>Pasenger Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Seat No</th>
                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>


                <tr>

                    <td>{{ $pasengerInfo->passenger_name }}</td>
                    <td>{{ $pasengerInfo->phone}}</td>
                    <td>{{ $pasengerInfo->email }}</td>
                    <td>{{ $pasengerInfo->seat_number }}</td>


                </tr>


                </tbody>
            </table>

        </div>
    </div>

@endsection
