<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhyUs extends Model
{
    protected $guarded = ['id'];

    protected $table = "why_uses";
}
