<?php

namespace App\Http\Controllers;

use App\FleetFacility;
use App\FleetRegistration;
use App\FleetType;
use App\TicketPrice;
use App\TripRoute;
use App\RideInfo;
use Illuminate\Http\Request;

class FleetController extends Controller
{
    public function fleetType()
    {

        $data['page_title'] = "FleetType List";
        $data['fleet_type'] = FleetType::latest()->paginate(20);
        return view('admin.fleet.fleet_type', $data);
    }

    public function fleetTypeStore(Request $request)
    {
        $request->validate([
            'name' => 'required|max:20'
        ]);
        $data = new FleetType();
        $data->name = $request->name;
        $data->status = $request->status == "on" ? 1 : 0;

        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }

    public function fleetTypeUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:20'
        ]);

        $data = FleetType::findOrFail($id);
        $data->name = $request->name;
        $data->status = $request->status == "on" ? 1 : 0;
        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }


    public function fleetFacility()
    {
        $data['page_title'] = "Fleet Facilities";
        $data['fleet_type'] = FleetFacility::latest()->paginate(20);
        return view('admin.fleet.fleet_facilities', $data);
    }

    public function fleetFacilityStore(Request $request)
    {
        $request->validate([
            'title' => 'required|max:40',
            'details' => 'required'
        ]);
        $data = new FleetFacility();
        $data->title = $request->title;
        $data->details = $request->details;
        $data->status = $request->status == "on" ? 1 : 0;

        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }

    public function fleetFacilityUpdate(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:40',
            'details' => 'required'
        ]);

        $data = FleetFacility::findOrFail($id);
        $data->title = $request->title;
        $data->details = $request->details;
        $data->status = $request->status == "on" ? 1 : 0;
        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }


    public function fleetRegistration()
    {

        $data['page_title'] = "Fleet Registration";
        $data['fleet'] = FleetRegistration::latest()->paginate(30);
        return view('admin.fleetreg.index', $data);
    }

    public function create()
    {

        $data['page_title'] = "New Fleet Registration";
        $data['fleet_type'] = FleetType::whereStatus(1)->get();
        $fleet_facility = FleetFacility::whereStatus(1)->get();

        $facility = [];
        foreach ($fleet_facility as $dd) {
            $facility[] = $dd->details;
        }

        return view('admin.fleetreg.create', $data, compact('facility'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'fleet_type_id' => 'required|numeric',
            'layout' => 'required',
            'fleet_facilities' => 'required',
        ]);

        $data = new FleetRegistration();
        $data->reg_no = $request->reg_no;
        $data->fleet_type_id = $request->fleet_type_id;
        $data->engine_no = $request->engine_no;
        $data->model_no = $request->model_no;
        $data->chasis_no = $request->chasis_no;
        $data->layout = $request->layout;

        $data->total_seat = $request->total_seat;

        $data->lastseat = ($request->lastseat == "on") ? 1 : 0;
        $data->ac_available = ($request->ac_available == "on") ? 1 : 0;
        $data->status = ($request->status == "on") ? 1 : 0;

        $data->seat_numbers = $request->seat_numbers;
        $data->fleet_facilities = $request->fleet_facilities;
        $data->owner = $request->owner;
        $data->company = $request->company;

        $bus_id = $data->save();
        $rideInfo = new RideInfo();
        $rideInfo->driver_name = $request->driver_name;
        $rideInfo->bus_id = $data->id;
        $rideInfo->driver_phone = $request->driver_phone;
        $rideInfo->superviser_phone = $request->superviser_phone;
        $rideInfo->save();
        if ($rideInfo) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }


    public function edit($id)
    {
        $fleet = FleetRegistration::findOrFail($id);
        $page_title = "Edit Fleet ";
        $fleet_type = FleetType::whereStatus(1)->get();
        $fleet_facility = FleetFacility::whereStatus(1)->get();

        $facility = [];
        foreach ($fleet_facility as $dd) {
            $facility[] = $dd->details;
        }
        return view('admin.fleetreg.edit', compact('facility', 'page_title', 'fleet_type', 'fleet'));
    }

    public function update(Request $request, $id)
    {
        //return $request;

        $request->validate([
            'fleet_type_id' => 'required|numeric',
            'layout' => 'required',
            'fleet_facilities' => 'required',
        ]);

        $data = FleetRegistration::findOrFail($id);
        $data->reg_no = $request->reg_no;
        $data->fleet_type_id = $request->fleet_type_id;
        $data->engine_no = $request->engine_no;
        $data->model_no = $request->model_no;
        $data->chasis_no = $request->chasis_no;
        $data->layout = $request->layout;

        $data->total_seat = $request->total_seat;

        $data->lastseat = ($request->lastseat == "on") ? 1 : 0;
        $data->ac_available = ($request->ac_available == "on") ? 1 : 0;
        $data->status = ($request->status == "on") ? 1 : 0;

        $data->seat_numbers = $request->seat_numbers;
        $data->fleet_facilities = $request->fleet_facilities;
        $data->owner = $request->owner;
        $data->company = $request->company;

        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }


    public function ticketPrice()
    {
        $data['page_title'] = "Ticket Price";
        $data['fleet_type'] = FleetType::where('status', 1)->get();
        $data['trip_route'] = TripRoute::where('status', 1)->orderBy('name', 'asc')->get();
        $data['ticketPrice'] = TicketPrice::latest()->paginate(30);
        return view('admin.fleet.ticket-price', $data);
    }


    public function ticketPriceStore(Request $request)
    {
        $request->validate([
            'trip_route_id' => 'required',
            'fleet_type_id' => 'required',
            'price' => 'required|numeric|min:0',
        ]);

        $checkTicketPrice = TicketPrice::where('trip_route_id', $request->trip_route_id)->where('fleet_type_id', $request->fleet_type_id)->count();
        if ($checkTicketPrice != 0) {
            $notification = array('message' => 'Already exist!', 'alert-type' => 'error');
            return back()->with($notification);
        }

        $data = new TicketPrice();
        $data->trip_route_id = $request->trip_route_id;
        $data->fleet_type_id = $request->fleet_type_id;
        $data->price = $request->price;
        $succ = $data->save();
        if ($succ) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something Wrong', 'alert-type' => 'error');
        }
        return back()->with($notification);
    }

    public function ticketPriceUpdate(Request $request, $id)
    {
        $request->validate([
            'trip_route_id' => 'required',
            'fleet_type_id' => 'required',
            'price' => 'required|numeric|min:0',
        ]);

        $data['trip_route_id'] = $request->trip_route_id;
        $data['fleet_type_id'] = $request->fleet_type_id;
        $data['price'] = $request->price;
        TicketPrice::where('id', $id)->update($data);;
        $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        return back()->with($notification);
    }

    public function ticketPriceDestroy($id)
    {
        $suc = TicketPrice::destroy($id);
        if ($suc) {
            $notification = array('message' => 'Deleted Successfully!', 'alert-type' => 'success');
        } else {
            $notification = array('message' => 'Something wrong!', 'alert-type' => 'error');
        }
        return back()->with($notification);

    }


}
