<?php

namespace App\Http\Controllers;

use App\Agent;
use App\FleetRegistration;
use App\TicketBooking;
use App\TripRoute;
use Auth;
use http\Env\Response;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function ticket_sale(Request $request){

        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $agent_id = $request->point_id;
        $trip_route_id = $request->route_id;
        $ticket_sales = new TicketBooking();

        if(!empty($agent_id))
            $ticket_sales = $ticket_sales->where('agent_id', $agent_id);

        if(!empty($trip_route_id))
            $ticket_sales = $ticket_sales->where('trip_route_id', $trip_route_id);

        if( !empty($start_date) )
            $start_date = db_date_format($start_date);

        if( !empty($end_date) )
            $end_date = db_date_format($end_date);


        if( empty($start_date) && (!empty($end_date)) ) {

            $ticket_sales = $ticket_sales->whereDate('booking_date','<=', $end_date);
        }elseif( (!empty($start_date)) && empty($end_date) ) {

            $ticket_sales = $ticket_sales->whereDate('booking_date','>=', $start_date);
        }elseif ( $start_date !='' && $end_date != '' ) {

            $ticket_sales = $ticket_sales->whereBetween('booking_date',[$start_date, $end_date]);
        }


        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['trip_routes'] = TripRoute::get()->pluck('name','id');
        $data['agents'] = Agent::get()->pluck( 'full_name','id');
        $data['page_title'] = "Ticker Sale Report";

        if($request->type=="print" || $request->type=="download") {

            $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->get();
            if ($request->type == "print") {
                return view('admin.report.print_ticket_sale', $data);
//            } else if ($request->type == "download") {
//                $pdf = PDF::loadView('member.reports.print_daily_stock_report', $data);
//                $file_name = file_name_generator($title);
//                return $pdf->download($file_name);
            }
        } else {


            $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->get();
            return view('admin.report.ticket_sales',$data);
        }

    }

    public function agent_ticket_sale(Request $request)
    {
      $start_date = $request->date;


      $agent_id = Auth::guard('agent')->user()->id;;
      $trip_route_id = $request->route_id;


      $ticket_sales = new TicketBooking();


      if(!empty($trip_route_id))
          $ticket_sales = $ticket_sales->where('trip_route_id', $trip_route_id)->where('agent_id',$agent_id);


      if( !empty($start_date) )
          $start_date = db_date_format($start_date);


      if( empty($trip_route_id) && (!empty($start_date)) ) {

          $ticket_sales = $ticket_sales->whereDate('booking_date', $start_date)->where('agent_id',$agent_id);


      }elseif( (!empty($trip_route_id)) && empty($start_date) ) {

          $ticket_sales = $ticket_sales->where('trip_route_id', $trip_route_id)->where('agent_id',$agent_id);;
      }

      $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
      $data['trip_routes'] = TripRoute::get()->pluck('name','id');
      $data['agents'] = Agent::get()->pluck( 'full_name','id');
      $data['page_title'] = "Ticker Sale Report";


      if($request->type=="print" || $request->type=="download") {

          $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->get();
          if ($request->type == "print") {
              return view('admin.report.print_ticket_sale', $data);
//            } else if ($request->type == "download") {
//                $pdf = PDF::loadView('member.reports.print_daily_stock_report', $data);
//                $file_name = file_name_generator($title);
//                return $pdf->download($file_name);
          }
      } else {


          $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->where('agent_id',$agent_id)->get();

          return view('agent.ticket_sale_report',$data);
      }


    }

    public function busPasengerInfo(Request $request)
    {
      $bus_id = $request->bus_id;
      $data['all_bus_reg'] = FleetRegistration::get()->pluck( 'reg_no','id');
      $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
      $data['page_title'] = "Passenger Information";
      $ticket_sales = new TicketBooking();

      if(empty($bus_id))
      {
          $data['passengers'] = $ticket_sales->select('passenger_name as name','email','phone','seat_number','total_fare')->where('payment_status',1)->where('cancel_req',0)->get();
      }
      else
      {
          $data['passengers'] = $ticket_sales->select('passenger_name as name','email','phone','seat_number','total_fare')->where('payment_status',1)->where('cancel_req',0)->where('fleet_registration_id',$bus_id)->get();
      }


        if ($request->type == "print") {
            return view('admin.report.print_pasenger_info', $data);
        }
        else {
            return view('admin.report.pasenger_info',$data);
        }


    }
}
