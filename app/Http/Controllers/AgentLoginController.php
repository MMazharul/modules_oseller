<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\GeneralSettings;
class AgentLoginController extends Controller
{
    public function __construct(){
        $Gset = GeneralSettings::first();
        $this->sitename = $Gset->sitename;
    }
    public function index(){

        if(Auth::guard('agent')->check()){
            return redirect()->route('agent.dashboard');
        }
        $data['page_title'] = "Agent Sign In";
        return view('agent.agentloginform', $data);
    }

    public function authenticate(Request $request){
        if (Auth::guard('agent')->attempt([
            'username' => strtolower(trim($request->username)),
            'password' => $request->password,
        ])){
            $user =  Auth::guard('agent')->user();
            if($user->status == 0){
                Auth::guard('agent')->logout();
                session()->flash('success', 'Your Account Has been Blocked!!');
                return redirect('/agent');
            }
            return redirect()->route('agent.dashboard');
        }
        session()->flash('error', 'Username Or Password don\'t match!!');
        return back();
    }
}
